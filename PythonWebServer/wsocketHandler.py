import wClient
import asyncore
import parsers
import globalVars
import constants
import struct
import random
import string

class WsocketHandler(asyncore.dispatcher_with_send):
    TEXT = 0x01
    BINARY = 0x02
    NAME = ""
    def genName(self):
        if (self.NAME == ""):
            self.NAME = "Client #%d" % (len(globalVars.wsock_clients))
            
    def handle_read(self):
        self.genName()
        data = self.recv(8192)
        if data == "":
            data = None
        if data is not None:
            print "[WSDATA FROM CLIENT BEGIN]\n"+data+"\n[WSDATA FORM CLIENT END]";
            try:
                request = parsers.HTTPRequest(data)
                acceptKey = parsers.WebsocketHandshake().parseKey(request.headers.getheader('sec-websocket-key'))
                response = constants.SWITCHING_PROTOCOLS_HEADER%(acceptKey)
                self.send(response)
            except:
                self.buffer = data
                data = self.parse_frame()
                if ".quit" in data:
                    DCMessage = self.prepare_send("Client has disconnected...Snake? Snake!? SNAAAAAKE!!!!")
                    wClient.EchoMessage(DCMessage)
                    self.handle_close()
                elif ".name" in data:
                    response = self.prepare_send(self.NAME+" changed his name to "+data.split(".name")[1])
                    self.NAME = data.split(".name")[1]
                    wClient.EchoMessage(response)
                elif ".getMyName" in data:
                    response = self.prepare_send("."+self.NAME)
                    self.send(response)
                else:
                    response = self.prepare_send(self.NAME+": "+data)
                    wClient.EchoMessage(response)
            #print "[DATA TO CLIENT BEGIN]\n"+response+"\n[DATA TO CLIENT END]"
            
    def echoFrame(self,message):
        self.send(message)
        
    def handle_close(self):
        self.close()
        self.closing = True
        wClient.isClosed()
        globalVars.connections-=1
        print "[Closing connection] %s:%d Connections Active: %d" % (self.addr[0],self.addr[1],globalVars.connections)
        
    def parse_frame(self):
        """
        Parse a WebSocket frame. If there is not a complete frame in the
        buffer, return without modifying the buffer.
        """
        buf = self.buffer
        payload_start = 2

        # try to pull first two bytes
        if len(buf) < 3:
            return
        b = ord(buf[0])
        fin = b & 0x80      # 1st bit
        # next 3 bits reserved
        opcode = b & 0x0f   # low 4 bits
        b2 = ord(buf[1])
        mask = b2 & 0x80    # high bit of the second byte
        length = b2 & 0x7f    # low 7 bits of the second byte

        # check that enough bytes remain
        if len(buf) < payload_start + 4:
            return
        elif length == 126:
            length, = struct.unpack(">H", buf[2:4])
            payload_start += 2
        elif length == 127:
            length, = struct.unpack(">I", buf[2:6])
            payload_start += 4

        if mask:
            mask_bytes = [ord(b) for b in buf[payload_start:payload_start + 4]]
            payload_start += 4

        # is there a complete frame in the buffer?
        if len(buf) < payload_start + length:
            return

        # remove leading bytes, decode if necessary, dispatch
        payload = buf[payload_start:payload_start + length]
        self.buffer = buf[payload_start + length:]

        # use xor and mask bytes to unmask data
        if mask:
            unmasked = [mask_bytes[i % 4] ^ ord(b)
                            for b, i in zip(payload, range(len(payload)))]
            payload = "".join([chr(c) for c in unmasked])

        if opcode == WsocketHandler.TEXT:
            s = payload.decode("UTF8")
            return s
        if opcode == WsocketHandler.BINARY:
            return payload
        
    def prepare_send(self, s):
        """
        Encode and send a WebSocket message
        """

        message = ""
        # always send an entire message as one frame (fin)
        b1 = 0x80

        # in Python 2, strs are bytes and unicodes are strings
        #if type(s) == unicode:
            #print "Srint"
            #b1 |= WsocketHandler.TEXT
            #payload = s.encode("UTF8")
        #if type(s) == str:
        b1 |= WsocketHandler.TEXT
        payload = s.encode("UTF8")
        message += chr(b1)
        # never mask frames from the server to the client
        b2 = 0
        length = len(payload)
        if length < 126:
            b2 |= length
            message += chr(b2)
        elif length < (2 ** 16) - 1:
            b2 |= 126
            message += chr(b2)
            l = struct.pack(">H", length)
            message += l
        else:
            l = struct.pack(">Q", length)
            b2 |= 127
            message += chr(b2)
            message += l
        message += payload
        return message
