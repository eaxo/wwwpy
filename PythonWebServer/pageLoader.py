from os import listdir
from os import path
import mimetypes
import constants
import re

DataFolder="www" 

def import_module(scriptName,action):
    mod = __import__(DataFolder+'.'+scriptName, fromlist=[scriptName])
    reload(mod)
    klass = getattr(mod, scriptName)
    inst = klass(action)
    return inst.CompiledSite()

class pageLoader(object):

    def __init__(self):
        mimetypes.init()
 
    def _getPyModuleName(self,pageName):
        re1='.*?'    # Non-greedy match on filler
        re2='((?:[a-z][a-z]+))'    # Word 1
        rg = re.compile(re1+re2,re.IGNORECASE|re.DOTALL)
        m = rg.search(pageName)
        if m:
            module=m.group(1)
        return module

    def loadPage(self,pageName,action):
        if(path.isdir(DataFolder+pageName)):
            if (not pageName.endswith("/")):
                pageName+="/"
        if(pageName.endswith("/")):
            try:
                message = import_module(self._getPyModuleName("index"),action)
                return constants.OK_HEADER % ("text/html",len(message), message)
            except:
                fileReader = open(DataFolder+pageName+"index.html","rb")
                message = fileReader.read()
                return constants.OK_HEADER % (mimetypes.guess_type(DataFolder+pageName),len(message), message)
        elif(pageName == "/%7Cdir%7C"):
            message = ""
            for files in listdir(DataFolder+"/."):
                message += "<a href="+files+">"+files+"</a><br>"
            return constants.OK_HEADER % ("text/html",len(message), message)
        else:
            if ".py" in pageName:
                message = import_module(self._getPyModuleName(pageName),action)
                return constants.OK_HEADER % ("text/html",len(message), message)
            elif ".html" in pageName:
                fileReader = open(DataFolder+pageName,"rb")
                message = fileReader.read()
                return constants.OK_HEADER % (mimetypes.guess_type(DataFolder+pageName),len(message), message)
            elif ".ico" in pageName:
                fileReader = open(DataFolder+pageName,"rb")
                message = fileReader.read()
                return constants.OK_HEADER % (mimetypes.guess_type(DataFolder+pageName),len(message), message)
            else:
                fileReader = open(DataFolder+pageName,"rb")
                message = fileReader.read()
                return constants.OK_CHUNKED_HEADER % (mimetypes.guess_type(DataFolder+pageName),len(message), message)
                