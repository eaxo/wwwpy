
import asyncore
import parsers
import globalVars

class WebHandler(asyncore.dispatcher_with_send):
    def handle_read(self):
        data = self.recv(8192)
        if data == "":
            data = None
        if data is not None:
            print "[DATA FROM CLIENT BEGIN]\n"+data+"\n[DATA FORM CLIENT END]";
            request = parsers.HTTPRequest(data)
            if(request.command == "POST"):
                len = request.headers.getheader('content-lenght')
                action =  request.rfile.read(len)
            else:
                action = None
            response = parsers.ParseHTML(request.path,action).file
            #print "[DATA TO CLIENT BEGIN]\n"+response+"\n[DATA TO CLIENT END]"
            self.send(response)
            
    def mysend(self, msg):
        totalsent = 0
        while totalsent < len(msg):
            print totalsent
            sent = self.send(msg[totalsent:])
            if sent == 0:
                raise RuntimeError("socket connection broken")
            totalsent = totalsent + int(sent)
       
            
    def handle_close(self):
        self.close()
        globalVars.connections-=1
        print "[Closing connection] %s:%d Connections Active: %d" % (self.addr[0],self.addr[1],globalVars.connections)