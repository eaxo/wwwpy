from BaseHTTPServer import BaseHTTPRequestHandler
from StringIO import StringIO
import pageLoader
import base64



try:
    import hashlib
    md5_hash = hashlib.md5
    sha1_hash = hashlib.sha1
except ImportError:
    import md5
    import sha
    md5_hash = md5.md5
    sha1_hash = sha.sha

WEBSOCKET_ACCEPT_UUID = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11'
movieFormat="(\.mp4$|\.avi$|\.wmv$|\.ogg$)"
        
class HTTPRequest(BaseHTTPRequestHandler):
    def __init__(self, request):
        self.rfile = StringIO(request)
        self.raw_requestline = self.rfile.readline()
        self.error_code = self.error_message = None
        self.parse_request()
                
    def send_error(self, code, message):
        self.error_code = code
        self.error_message = message

class WebsocketHandshake():
    def parseKey(self,key):
        accept_binary = sha1_hash(key + WEBSOCKET_ACCEPT_UUID).digest()
        return (base64.b64encode(accept_binary))
    
class ParseHTML():         
    def _splitData(self,pageName):
        finalPageName = pageName
        action = ""
        if(pageName.__contains__("?")):
            finalPageName,action = pageName.split("?")
            action = action.split("&")
        return finalPageName,action
    
    def __init__(self,pageName,postData):
        if postData is None:
            pageName,postData = self._splitData(pageName)
        page = pageLoader.pageLoader()
        self.file = page.loadPage(pageName,postData)