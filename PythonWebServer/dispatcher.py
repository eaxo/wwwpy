from webhandler import WebHandler
from wsocketHandler import WsocketHandler
import globalVars
import asyncore
import socket


class EchoServer(asyncore.dispatcher):
    def __init__(self,host,port,type):
        self.type = type
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind((host,port))
        self.listen(5)
        
    def handle_accept(self):
        pair = self.accept()
        if pair is not None:
            sock, addr = pair
            if(self.type == "http"):
                handler  = WebHandler(sock)
                print "[Acception httpConnection]"
            elif(self.type == "whttp"):
                globalVars.wsock_clients.append(WsocketHandler(sock));
                print globalVars.wsock_clients
                print "[Acception wSocketConnection]"
            globalVars.connections+=1
            print "[Accepting connection] %s:%d Connections Active: %d" % (addr[0],addr[1],globalVars.connections)

server = {}
server[0] = EchoServer("0.0.0.0",1911,"http")
server[1] = EchoServer("0.0.0.0",1912,"whttp")
asyncore.loop()