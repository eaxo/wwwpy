'''
Created on 31 okt 2013

@author: Efildru
'''
import random
import string

class index(object):        
    def _opVer(self):
        return "Version: 0.1a"
   
    def CompiledSite(self):
        return str(self.SiteCode)
    def _createStringFromArgs(self,args):
        if(len(args)>0):
            key,value = args[0].split("=")
            dict = {key:value}
            compiledStr=""
            for i in range(1,len(args)):
                key,value =args[i].split("=")
                dict[key] = value
            for key,value in dict.items():
                compiledStr += key+": "+value + "<p>"
            return compiledStr
        else:
            return "None"
    
    def __init__(self,args):
        chars = "".join( [random.choice(string.letters[:26]) for i in xrange(15)] )
        self.SiteCode = """
        <!DOCUMENT html>
        <head>
            <title>OpScript """+ self._opVer()+"""</title>
        </head>
        <body>
            <center>Random Characters: """+chars+"""<p>Current input:<p>"""+self._createStringFromArgs(args)+"""
            </center>
            <form name="input" action="index.html" method="post">
                Username: <input type="text" name="user">
                Password: <input type="password" name="password">
                FirstName: <input type="text" name="firstname">
                LastName: <input type="text" name="lastname">
                Street: <input type="text" name="street">
                WEHHH: <input type="text" name="weh">
                <input type="submit" value="Submit">
            </form>
        </body>
        """
        