OK_HEADER = "HTTP/1.1 200 OK\r\nServer: pyWeb/0.1(custom)\r\nContent-Type: %s \r\nContent-Length: %d\r\nConnection: keep-alive\r\n\r\n%s"
OK_CHUNKED_HEADER = "HTTP/1.1 200 OK\r\nServer: pyWeb/0.1(custom)\r\nContent-Type: %s \r\nTransfer-Encoding:chunked\r\nConnection: keep-alive\r\n\r\n%s"
SWITCHING_PROTOCOLS_HEADER = "HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept:%s\r\n\r\n"
NOT_FOUND_HEADER = "HTTP/1.1 404 Not Found\r\nServer: pyWeb/0.1(custom)\r\nContent-Type: text/html \r\nContent-Length: %d\r\nConnection: close\r\n\r\n%s"